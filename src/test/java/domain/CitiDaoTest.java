package domain;

import domain.CitiDao;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import repository.Citi;
import service.WeatherResponse;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CitiDaoTest {
//    public CitiDao citiDao=new CitiDao();

    @Test
    void saveCiti(){
        Citi citi= CitiDao.saveCiti(new WeatherResponse());
//        Assertions.assertThat(citi).isEqualTo(new Citi());
        assertEquals(citi,new Citi());
    }

    @Test
    void allCities(){
        List<Citi> cities=CitiDao.allCities();
//        Assertions.assertThat(cities.hashCode()).isEqualTo(552061367);
//        List<Citi> cities2=new ArrayList<>();
//
//        cities2.add(new Citi(32,"Cluj","RO","RO",46.6494,23.8184));
//        cities2.add(new Citi(33,"Suceava","RO","RO",47.6333,26.25));
        assertEquals(cities.hashCode(),941458293);
    }


}