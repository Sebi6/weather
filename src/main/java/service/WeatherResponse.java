package service;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

public class WeatherResponse {
    private int timezone;
    private int id;
    private String name;
    private int cod;
    private int dt;
    private int visibility;
    private String base;

    @SerializedName("clouds")
    private Cloud cloud;

    @SerializedName("weather")
    private List<DescriptionWeather> descriptionWeather;

    @SerializedName("coord")
    private GeoLocation geoLocation;

    @SerializedName("main")
    private WeatherCondition weatherCondition;

    @SerializedName("wind")
    private Wind wind;

    public WeatherCondition getWeatherCondition() {
        return weatherCondition;
    }

    public void setWeatherCondition(WeatherCondition weatherCondition) {
        this.weatherCondition = weatherCondition;
    }

    @SerializedName("sys")
    private Sys sys;

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public GeoLocation getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.geoLocation = geoLocation;
    }

    public List<DescriptionWeather> getWeather() {
        return descriptionWeather;
    }

    public void setWeather(List<DescriptionWeather> descriptionWeather) {
        this.descriptionWeather = descriptionWeather;
    }

    public WeatherCondition getMain() {
        return weatherCondition;
    }

    public void setMain(WeatherCondition weatherCondition) {
        this.weatherCondition = weatherCondition;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }



    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    @Override
    public String toString() {
        return "WeatherResponse{" +
                "timezone=" + timezone +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", dt=" + dt +
                ", visibility=" + visibility +
                ", base='" + base + '\'' +
                ", geoLocation=" + geoLocation +
                ", weather=" + descriptionWeather +
                ", WeatherCondition=" + weatherCondition +
                ", Cloud=" + cloud +
                ", wind=" + wind +
                ", sys=" + sys +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeatherResponse)) return false;
        WeatherResponse that = (WeatherResponse) o;
        return getTimezone() == that.getTimezone() && getId() == that.getId() && getCod() == that.getCod() && getDt() == that.getDt() && getVisibility() == that.getVisibility() && Objects.equals(getName(), that.getName()) && Objects.equals(getBase(), that.getBase()) && Objects.equals(cloud, that.cloud) && Objects.equals(descriptionWeather, that.descriptionWeather) && Objects.equals(getGeoLocation(), that.getGeoLocation()) && Objects.equals(getWeatherCondition(), that.getWeatherCondition()) && Objects.equals(getWind(), that.getWind()) && Objects.equals(getSys(), that.getSys());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimezone(), getId(), getName(), getCod(), getDt(), getVisibility(), getBase(), cloud, descriptionWeather, getGeoLocation(), getWeatherCondition(), getWind(), getSys());
    }
}

