package repository;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "weather")
public class Weather {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int weatherId;
    private LocalDateTime date;
    private double temperature;
    private double pressure;
    private double humidity;
    @Column(name = "wind")
    private double wind;
    private double speed;

    @ManyToOne
    @JoinColumn(name = "citiId")
    private Citi citi;

    public Citi getCiti() {
        return citi;
    }

    public void setCiti(Citi citi) {
        this.citi = citi;
    }

    public Weather() {
    }

    public Weather(int weatherId, LocalDateTime date, int temperature, int pressure, int humidity, int windDirection, int speed) {
        this.weatherId = weatherId;
        this.date = date;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.wind= windDirection;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "weatherId=" + weatherId +
                ", date=" + date +
                ", temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", wind='" + wind + '\'' +
                ", speed=" + speed +
                '}';
    }

    public int getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getPressure() {
        return this.pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getWind() {
        return wind;
    }

    public void setWind(double wind) {
        this.wind = wind;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
