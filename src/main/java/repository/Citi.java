package repository;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cities")
public class Citi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int citiId;
    private String name;
    private String country;
    private String region;
    private double longitude;
    private double latitude;

    @OneToMany(mappedBy = "citi")
    private List<Weather> weathers;

    public Citi() {
    }

    public int getCitiId() {
        return citiId;
    }

    public void setCitiId(int citiId) {
        this.citiId = citiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Citi(int citiId, String name, String country, String region, double longitude, double latitude) {
        this.citiId = citiId;
        this.name = name;
        this.country = country;
        this.region = region;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Citi{" +
                "citiId=" + citiId +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Citi)) return false;
        Citi citi = (Citi) o;
        return getCitiId() == citi.getCitiId() && Double.compare(citi.getLongitude(), getLongitude()) == 0 && Double.compare(citi.getLatitude(), getLatitude()) == 0 && Objects.equals(getName(), citi.getName()) && Objects.equals(getCountry(), citi.getCountry()) && Objects.equals(getRegion(), citi.getRegion()) && Objects.equals(weathers, citi.weathers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCitiId(), getName(), getCountry(), getRegion(), getLongitude(), getLatitude(), weathers);
    }
}
