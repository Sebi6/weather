package app;

import com.sun.xml.internal.bind.v2.TODO;
import domain.ApiResponse;
import domain.CitiDao;
import domain.WeatherDao;
import net.bytebuddy.asm.Advice;
import repository.Citi;
import repository.Weather;
import service.WeatherResponse;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Menu {
    static Scanner scan = new Scanner(System.in);
    public static void showMenu(){
        String option;
        do{
            System.out.println("Weather apk.\n");
            System.out.println("1.Add a city.\n" +
                               "2.Show all city's.\n" +
                               "3.Show weather by a specific data.\n" +
                               "4.Exit\n");
            System.out.print("option:");
            option = scan.nextLine();
            switch (option){
                case "1":
                    option1();
                    break;
                case "2":
                    option2();
                    break;
                case "3":
                    option3();// TODO: 14.07.2021 se apeleaza si default
                    break;
                case "4":
                    System.out.println("Bye Bye...\n");
                    break;
                default:
                    System.out.println("Impute a valid option or press 4 for exit...\n");
                    try {
                        TimeUnit.SECONDS.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }while(!option.equalsIgnoreCase("4"));
    }

     public static void option1(){
        System.out.print("city:");
        String city = scan.nextLine();
        try {
            WeatherResponse weatherResponse = ApiResponse.apiResponse(city);
            if(weatherResponse.getCod()!=200){
                System.out.println("This city is not exist...");
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }else{
               Citi currentCity = CitiDao.saveCiti(weatherResponse);
               WeatherDao.saveWeatherResponse(weatherResponse,currentCity);
               showWeather(weatherResponse,currentCity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void showWeather(WeatherResponse weatherResponse,Citi currentCity){
        System.out.println(currentCity.getName()+"----------"+LocalDateTime.now() +" \n" +
                "-----------------------------\n" +
                "temperature : "+((int)weatherResponse.getWeatherCondition().getTemp())+":grade Celsius"+"\n" +
                "pressure : "+((int)weatherResponse.getWeatherCondition().getPressure())+"hPa"+"\n"+
                "humidity : "+((int)weatherResponse.getWeatherCondition().getHumidity())+"%"+"\n"+
                "wind direction : "+weatherResponse.getWind().getDeg()+"\n"+
                "wind speed : "+weatherResponse.getWind().getSpeed());
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void option2(){
        if (CitiDao.allCities()!=null) {
            int i = 1;
            for(Citi c: Objects.requireNonNull(CitiDao.allCities())){
                System.out.println((i++)+". "+c.getName()+" / "+c.getCountry());
            }
        }else {
            System.out.println("There is no city in the database");
        }
    }
    public static void option3(){
        //orasul+data de afisat
        //primim oras->ferificam daca exista-> daca nu revenim la meniu
        //-> daca da intreba, data->daca exista data afisam vremea, daca nu, oferim un mesaj corespunzator
        System.out.print("city:");
        String currentCity = scan.nextLine();
        int year,month,day;
        Citi city = new Citi();
        int counter = 0;
        LocalDateTime date;
        if (CitiDao.allCities().size() != 0) {
            for (Citi c : Objects.requireNonNull(CitiDao.allCities())) {
                if (c.getName().equalsIgnoreCase(currentCity)) {// TODO: 14.07.2021 am adaugat ignore case 
                    city = c;
                    counter++;
                }
            }
            if(counter == 1){// TODO: 7/14/2021 functii separate de citire pentru fiecare in parte
                System.out.println("input date:");
                System.out.print("year:");
                year = scan.nextInt();
                System.out.print("month:");
                month= scan.nextInt();
                System.out.print("day:");
                day= scan.nextInt();
                date = LocalDateTime.of(year,month,day,00,00);
                findDate(city,date);

            }else{
                System.out.println("This city not exist in our database...");
            }
        } else {
            System.out.println("Database is empty...");
        }
    }
    public static void findDate(Citi city, LocalDateTime date){
        int counter = 0;
        for(Weather w:WeatherDao.allWeatherRecords()){
            if(date.toLocalDate().isEqual(w.getDate().toLocalDate()) && city.getCitiId() == w.getCiti().getCitiId()) {
                System.out.println(city.getName() + "----------" + date + " \n" +
                        "-----------------------------\n" +
                        "temperature : " +((int) w.getTemperature()) +":grade Celsius"+"\n" +
                        "pressure : " + ((int)w.getPressure()) +"hPa"+ "\n" +
                        "humidity : " + ((int)w.getHumidity() )+"%"+ "\n" +
                        "wind direction : " + w.getWind() + "\n" +
                        "wind speed : " + w.getSpeed()); // TODO: 14.07.2021 ar putea fi considerat cod duplicat
                counter++;
                break;
            }
        }
        if(counter== 0){
            System.out.println("We don't have records about this day...");
        }
//        System.out.println(counter);
    }
}
