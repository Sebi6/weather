package app;

import app.Menu;
import com.google.gson.Gson;
import domain.ApiResponse;
import domain.CitiDao;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import repository.HibernateUtil;
import repository.Weather;
import service.WeatherResponse;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {


        Menu.showMenu();
        HibernateUtil.shutdown();
    }
}
