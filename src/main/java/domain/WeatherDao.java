package domain;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import repository.Citi;
import repository.HibernateUtil;
import repository.Weather;
import service.WeatherResponse;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class WeatherDao {
    public static void saveWeatherResponse(WeatherResponse weatherResponse,Citi city) {
        Transaction transaction = null;
        try {
            Weather currentWeather = new Weather();
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            currentWeather.setDate(LocalDateTime.now());
            currentWeather.setTemperature((weatherResponse.getWeatherCondition().getTemp()));
            currentWeather.setPressure(weatherResponse.getWeatherCondition().getPressure());
            currentWeather.setHumidity(weatherResponse.getWeatherCondition().getHumidity());
            currentWeather.setWind(weatherResponse.getWind().getDeg());
            currentWeather.setSpeed(weatherResponse.getWind().getSpeed());
            currentWeather.setCiti(city);
            //System.out.println(currentWeather);
            session.save(currentWeather);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }
    public static List<Weather> allWeatherRecords() {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Weather");
            List<Weather> weather = query.list();
            return weather;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}