package domain;

import com.google.gson.Gson;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import service.WeatherResponse;

import java.io.IOException;

public class ApiResponse {
    public static final String API_KEY = "0a9dfebba2cf6a1c4dc976a46687c248";
    private static OkHttpClient client=new OkHttpClient();

    public static WeatherResponse apiResponse(String city) throws IOException {
        Request request=new Request.Builder()
                .url("https://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=" + API_KEY)
                .get()
                .build();

        Response response=client.newCall(request).execute();
        String responseJson=response.body().string();

        Gson gson=new Gson();
        WeatherResponse weatherResponse=gson.fromJson(responseJson,WeatherResponse.class);
        return weatherResponse;

    }

}
