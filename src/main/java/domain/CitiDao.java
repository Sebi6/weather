package domain;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import repository.Citi;
import repository.HibernateUtil;
import service.WeatherResponse;

import java.util.List;
import java.util.Objects;

public class CitiDao {
    public static Citi saveCiti(WeatherResponse weatherResponse) {
        Transaction transaction = null;
        Citi currentCity = new Citi();
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            currentCity.setName(weatherResponse.getName());
            currentCity.setCountry(weatherResponse.getSys().getCountry());
            currentCity.setRegion(weatherResponse.getSys().getCountry());
            currentCity.setLatitude(weatherResponse.getGeoLocation().getLongitude());
            currentCity.setLongitude(weatherResponse.getGeoLocation().getLatitude());
            int counter = 0;
            if (allCities().size() != 0) {
                for (Citi c : Objects.requireNonNull(allCities())) {
                    if (c.getName().equals(currentCity.getName())) {
                        currentCity = c;
                        counter++;
                        System.out.println("This city already exist...");
                    }
                }
                if (counter == 0) {
                    session.save(currentCity);
                    System.out.println("This city was added...");
                }
            } else {
                session.save(currentCity);
                System.out.println("This city was added...");
            }
            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return currentCity;
    }

    public static List<Citi> allCities() {
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            Query query = session.createQuery("from Citi");
            List<Citi> cities = query.list();
            return cities;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

//    public static void update
}